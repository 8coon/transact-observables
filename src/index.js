/* istanbul ignore file */

const {
	observable,
	observer,
	attachView,
	dispose,
} = require('../src/observable');

module.exports = {
	observable,
	observer,
	attachView,
	dispose,
};
