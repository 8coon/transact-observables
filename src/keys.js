
function makeSymbol(name) {
	/* istanbul ignore else */
	if (window.Symbol !== void 0) {
		return Symbol(name);
	}

	/* istanbul ignore next */
	return `$$${name}`;
}

module.exports = {
	attrsKey: makeSymbol('attrs'),
	administratorKey: makeSymbol('administrator'),
	viewsKey: makeSymbol('view'),
	observerId: makeSymbol('observerId'),
	reactionKey: makeSymbol('reaction'),
	disposedKey: makeSymbol('disposed'),
};
