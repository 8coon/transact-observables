'use strict';

const {attrsKey, viewsKey} = require('./keys');

let __callQueue = [];


function callReactions(observers) {
	const observersCalled = {};
	const callQueue = __callQueue;
	let observerId;
	__callQueue = [];

	// Вызываем observable из очереди
	for (let i = 0; i < callQueue.length; i++) {
		observerId = callQueue[i];

		if (!observersCalled[observerId]) {
			observersCalled[observerId] = true;

			if (observers[observerId]) {
				observers[observerId]();
			}
		}
	}

	__callQueue = [];
}


function makeReadUncommittedAdministrator(object, observableStack, observers) {
	return {
		trackedAttrs: {},
		observers: [],

		trackHook(name) {
			const attrs = object[attrsKey];
			const views = object[viewsKey];
			let attrObservers = this.trackedAttrs[name];
			let lastObservable;

			// Если мы внутри observable
			if (observableStack.length) {
				lastObservable = observableStack[observableStack.length - 1];

				// Сохраняем id текущей observable в trackedAttrs
				if (!attrObservers) {
					this.trackedAttrs[name] = [lastObservable];
				} else {
					attrObservers.push(lastObservable);
				}
			}

			if (views[name]) {
				// Возвращаем значение view
				return views[name].getter();
			}

			return attrs[name];
		},

		reactHook(name, value, viewComputationStack, actionCount) {
			const attrs = object[attrsKey];

			// Обновляем значение, если не вычисляем computed value
			if (!viewComputationStack) {
				attrs[name] = value;
			}

			const attrsObservers = this.trackedAttrs[name];

			if (attrsObservers) {
				// Добавляем в очередь вызовов observers на этот атрибут
				this.trackedAttrs[name] = null;
				Array.prototype.push.apply(__callQueue, attrsObservers);
			}

			// Если не в экшне, вызовем реакции
			if (!actionCount) {
				callReactions(observers);
			}
		},

		callReactionsHook() {
			callReactions(observers);
		},
	};
}


module.exports = {
	makeReadUncommittedAdministrator,
};
