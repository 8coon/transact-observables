'use strict';

const {
	attrsKey,
	administratorKey,
	viewsKey,
	observerId,
	reactionKey,
	disposedKey,
} = require('./keys');

const {makeReadUncommittedAdministrator} = require('./read-uncommitted');


const __observers = {};
const __observableStack = [];
let __lastObservableId = 1;
let __actionCount = 0;
let __viewComputationStack = 0;


function makeView(object, name, view, asMethod) {
	// Описание view
	const viewData = object[viewsKey][name] = {
		valid: false,
		cached: null,
		observer: null,
	};

	// Реакция, которая сбрасывает кеш
	const clearReaction = function $clearReaction() {
		viewData.cached = null;
		viewData.valid = false;

		// Распространяем реакцию
		__viewComputationStack++;
		object[administratorKey].reactHook(name, viewData.cached, __viewComputationStack, __actionCount);
		__viewComputationStack--;
	};

	// Реакция, которая пересчитает заново значение и кладёт в кеш
	const computeReaction = function $computeReaction() {
		viewData.cached = view.apply(object, arguments);
		// Валидными в кеше считаем только вычисляемые свойства без аргументов
		viewData.valid = arguments.length === 0;
	};

	const viewObserver = viewData.observer = observer(clearReaction);

	// Геттер для view
	viewData.getter = function $view() {
		// Если кеш невалиден, вызовем временный observer, который пересчитает заново значение и положит в кеш
		if (!viewData.valid) {
			setObserverReaction(viewObserver, computeReaction);
			viewObserver.apply(this, arguments);
			setObserverReaction(viewObserver, clearReaction);
		}

		// Возвратим значение из кеша
		return viewData.cached;
	};

	if (asMethod) {
		// Делаем метод
		Object.defineProperty(object, name, {
			enumerable: false,
			writable: false,
			value: viewData.getter,
		});
	} else {
		// Делаем атом-заглушку
		makeAtom(object, name, null);
	}
}


function makeAtom(object, name, defaultValue) {
	// Значение по умолчанию
	object[attrsKey][name] = defaultValue;

	Object.defineProperty(object, name, {
		get() {
			if (object[disposedKey]) {
				throw new Error('Cannot read from a disposed object!');
			}

			const administrator = object[administratorKey];
			return administrator.trackHook(name);
		},

		set(value) {
			if (object[disposedKey]) {
				throw new Error('Cannot mutate a disposed object!');
			}

			const attrs = object[attrsKey];
			const views = object[viewsKey];
			const administrator = object[administratorKey];

			if (views[name]) {
				// Нельзя изменять computed-свойство
				throw new Error(`Cannot mutate view '${name}'!`);
			}

			// Если значение не поменялось
			if (attrs[name] === value) {
				return;
			}

			administrator.reactHook(name, value, __viewComputationStack, __actionCount);
		},
	});
}


function makeAction(object, name, action) {
	Object.defineProperty(object, name, {
		enumerable: false,
		writable: false,

		value: function $action() {
			__actionCount++;
			action.apply(object, arguments);
			__actionCount--;

			// После последнего экшна запускаем реакции
			if (!__actionCount) {
				object[administratorKey].callReactionsHook();
			}
		}
	});
}


function observable(object) {
	if (!object || typeof object !== 'object') {
		throw new Error(`Expected an object, bun got '${typeof object}'`);
	}

	if (object[disposedKey]) {
		throw new Error('Cannot create observable over a disposed object!');
	}

	if (object[administratorKey]) {
		throw new Error('Cannot create observable on top of another observable!');
	}

	object[attrsKey] = {};
	object[viewsKey] = {};
	object[administratorKey] = makeReadUncommittedAdministrator(object, __observableStack, __observers);

	const keys = Object.keys(object);
	let key;
	let descriptor;

	// Проходимся по всем ключам объекта
	for (let i = 0; i < keys.length; i++) {
		key = keys[i];
		descriptor = Object.getOwnPropertyDescriptor(object, key);

		if (descriptor.get) {
			// View без аргументов (computed property)
			makeView(object, key, descriptor.get, false);
		} else if (typeof descriptor.value === 'function') {
			// Экшн
			makeAction(object, key, descriptor.value);
		} else {
			// Атом (обычный атрибут)
			makeAtom(object, key, descriptor.value);
		}
	}

	return object;
}


function observer(reaction) {
	if (typeof reaction !== 'function') {
		throw new Error(`Expected function, but got '${typeof reaction}'`);
	}

	const id = __lastObservableId++;

	const observe = function $observe() {
		__observableStack.push(id);
		const result = observe[reactionKey].apply(this, arguments);
		__observableStack.pop();

		return result;
	};

	// Сохраним observer в глобальной карте
	__observers[id] = observe;
	// Сохраняем id observer-а
	observe[observerId] = id;
	observe[reactionKey] = reaction;

	return observe;
}


function disposeObserver(observe) {
	setObserverReaction(observe, function $disposedObserverGuardReaction() {
		throw new Error('Attempt to call a disposed observer!');
	});

	__observers[observe[observerId]] = null;
}

function setObserverReaction(observe, reaction) {
	observe[reactionKey] = reaction;
}

function dispose(object) {
	if (typeof object === 'function') {
		// Снимаем observer
		disposeObserver(object);
		return;
	}

	if (!object || typeof object !== 'object' || !object[viewsKey]) {
		return;
	}

	// Снимаем observers от views
	Object.keys(object[viewsKey]).forEach(name => {
		const viewData = object[viewsKey][name];
		disposeObserver(viewData.observer);
	});

	// Выставляем флаг disposed
	object[disposedKey] = true;
}


module.exports = {
	observable,
	observer,
	attachView(object, name, view) {
		return makeView(object, name, view, true)
	},
	dispose,
	// Только для тестов
	__getAllObservers() {
		return __observers;
	}
};
